import React, { Component } from 'react'


class DaftarBuah extends Component {

    constructor(props) {
        super(props)
        this.state = {
            dataHargaBuah: [
                { nama: "Semangka", harga: 10000, berat: 1000 },
                { nama: "Anggur", harga: 40000, berat: 500 },
                { nama: "Strawberry", harga: 30000, berat: 400 },
                { nama: "Jeruk", harga: 30000, berat: 1000 },
                { nama: "Mangga", harga: 30000, berat: 500 }
            ],
            inputNama: "",
            inputHarga: "",
            inputBerat: "",
            index: -1
        }
        //bind method if method using normal function
        this.submitForm = this.submitForm.bind(this);
    }

    submitForm(event) {
        event.preventDefault()
        var index = this.state.index
        if (index === -1) {
            var newDataBuah = this.state.dataHargaBuah
            newDataBuah.nama = this.state.inputNama
            newDataBuah.harga = this.state.inputHarga
            newDataBuah.berat = this.state.inputBerat

            this.setState({
                dataHargaBuah: [...this.state.dataHargaBuah, newDataBuah],
                inputNama: "",
                inputHarga: "",
                inputBerat: ""
            })
        } else {
            var newDataBuah = this.state.dataHargaBuah
            newDataBuah[index].nama = this.state.inputNama
            newDataBuah[index].harga = this.state.inputHarga
            newDataBuah[index].berat = this.state.inputBerat

            this.setState({
                dataHargaBuah: [...newDataBuah],
                inputNama: "",
                inputHarga: "",
                inputBerat: "",
                index: -1
            })
        }
    }

    //arrow method no need bind
    changeInputNama = (event) => {
        var value = event.target.value
        this.setState({ inputNama: value })
    }

    changeInputHarga = (event) => {
        var value = event.target.value
        this.setState({ inputHarga: value })
    }

    changeInputBerat = (event) => {
        var value = event.target.value
        this.setState({ inputBerat: value })
    }

    deleteData = (event) => {
        var index = event.target.value
        var newDataBuah = this.state.dataHargaBuah
        newDataBuah.splice(index, 1)
        this.setState({
            dataHargaBuah: [...newDataBuah],
            inputNama: "",
            inputHarga: "",
            inputBerat: "",
            index: -1
        })
    }

    editData = (event) => {
        var index = event.target.value
        var namaBuah = this.state.dataHargaBuah[index].nama
        var hargaBuah = this.state.dataHargaBuah[index].harga
        var beratBuah = this.state.dataHargaBuah[index].berat
        this.setState({
            inputNama: namaBuah,
            inputHarga: hargaBuah,
            inputBerat: beratBuah,
            index
        })
    }





    render() {
        return (
            <>
                <h1 style={{ textAlign: "center" }}> Daftar Harga Buah</h1>
                <table style={{ border: "1px solid", width: "40%", margin: "0 auto" }}>
                    <thead style={{ background: "#aaa" }}>
                        <tr>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Berat</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody style={{ background: "coral" }}>
                        {this.state.dataHargaBuah.map((val, index) => {
                            return (
                                <>
                                    <tr>
                                        <td>{val.nama}</td>
                                        <td>{val.harga}</td>
                                        <td>{val.berat / 1000} kg</td>
                                        <td>
                                            <button value={index} onClick={this.editData} style={{ marginright: "3px", marginLeft: "5px" }}>Edit</button>
                                            <button value={index} onClick={this.deleteData}>Delete</button>
                                        </td>
                                    </tr>
                                </>
                            )
                        })}
                    </tbody>
                </table>
                <br /> <br />
                <h1 style={{ textAlign: "center" }}>Form Data Buah</h1>
                <form onSubmit={this.submitForm}>
                    <table style={{ width: "40%", margin: "0 auto" }}>
                        <tr>
                            <th>Nama Buah</th>
                            <td><input required type="text" value={this.state.inputNama} onChange={this.changeInputNama} /></td>
                        </tr>
                        <tr>
                            <th>Harga Buah</th>
                            <td><input required type="number" value={this.state.inputHarga} onChange={this.changeInputHarga} /></td>
                        </tr>
                        <tr>
                            <th>Berat Buah (gr)</th>
                            <td><input required type="number" value={this.state.inputBerat} onChange={this.changeInputBerat} /></td>
                        </tr>
                        <tr>
                            <td><br /></td>
                        </tr>
                        <tr>
                            <td><button class="button4">Submit</button></td>
                        </tr>
                    </table>
                </form>
            </>
        )
    }
}

export default DaftarBuah
