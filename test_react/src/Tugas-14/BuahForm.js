import React, { useContext } from "react";
import axios from 'axios'
import { BuahContext } from "./BuahContext";

const BuahForm = () => {
    const [dataHargaBuah, setDataHargaBuah, input, setInput] = useContext(BuahContext)

    const submitForm = (event) => {
        event.preventDefault()
        var newId = dataHargaBuah.lenght + 1

        if (input.id === null) {
            axios.post(`http://backendexample.sanbercloud.com/api/fruits`, { id: newId, name: input.name, price: input.price, weight: parseInt(input.weight) })
                .then(res => {
                    var data = res.data
                    setDataHargaBuah([...dataHargaBuah, { id: newId, name: data.name, price: data.price, weight: data.weight }])
                    setInput({ id: null, name: "", price: "", weight: "" })

                })
        } else {
            axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, { name: input.name, price: input.price, weight: parseInt(input.weight) })
                .then(res => {
                    var newDataBuah = dataHargaBuah.map(x => {
                        if (x.id === input.id) {
                            x.name = input.name
                            x.price = input.price
                            x.weight = input.weight
                        }
                        return x
                    })
                    setDataHargaBuah([...newDataBuah])
                    setInput({ id: null, name: "", price: "", weight: "" })
                })

        }
    }

    const changeInput = (event) => {
        var value = event.target.value
        setInput({ ...input, [event.target.name]: value })
    }

    return (
        <>
            <h1 style={{ textAlign: "center" }}>Form Data Buah</h1>
            <form onSubmit={submitForm}>
                <table style={{ width: "40%", margin: "0 auto" }}>
                    <tr>
                        <th>Nama Buah</th>
                        <td><input required type="text" name="name" value={input.name} onChange={changeInput} /></td>
                    </tr>
                    <tr>
                        <th>Harga Buah</th>
                        <td><input required type="number" name="price" value={input.price} onChange={changeInput} /></td>
                    </tr>
                    <tr>
                        <th>Berat Buah (gr)</th>
                        <td><input required type="number" name="weight" value={input.weight} onChange={changeInput} /></td>
                    </tr>
                    <tr>
                        <td><br /></td>
                    </tr>
                    <tr>
                        <td><button className="button4">Submit</button></td>
                    </tr>
                </table>
            </form>
        </>
    )

}

export default BuahForm