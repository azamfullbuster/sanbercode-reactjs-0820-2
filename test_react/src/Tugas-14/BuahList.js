import React, { useContext } from "react";
import axios from 'axios'
import { BuahContext } from "./BuahContext";

const BuahList = () => {
    const [dataHargaBuah, setDataHargaBuah, input, setInput] = useContext(BuahContext)

    const deleteData = (event) => {
        var idBuah = parseInt(event.target.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
            .then(res => {
                var newDataBuah = dataHargaBuah.filter(x => x.id !== idBuah)
                setDataHargaBuah([...newDataBuah])
            })

    }

    const editData = (event) => {
        var idBuah = parseInt(event.target.value)
        var buah = dataHargaBuah.find(x => x.id === idBuah)

        setInput({ ...input, id: idBuah, name: buah.name, price: buah.price, weight: buah.weight })

    }

    return (
        <>
            <h1 style={{ textAlign: "center" }}> Daftar Harga Buah</h1>
            <table style={{ border: "1px solid", width: "50%", margin: "0 auto" }}>
                <thead style={{ background: "#aaa" }}>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody style={{ background: "coral" }}>
                    {
                        dataHargaBuah !== null && dataHargaBuah.map((item, index) => {
                            return (
                                <>
                                    <tr key={item.id}>
                                        <td>{index + 1}</td>
                                        <td>{item.name}</td>
                                        <td>{item.price}</td>
                                        <td>{item.weight / 1000} kg</td>
                                        <td style={{ textAlign: "center" }}>
                                            <button value={item.id} onClick={editData} style={{ marginright: "3px", marginLeft: "5px" }}>Edit</button>
                                            <button value={item.id} onClick={deleteData}>Delete</button>
                                        </td>
                                    </tr>
                                </>
                            )
                        })
                    }
                </tbody>
            </table>
        </>
    )
}

export default BuahList