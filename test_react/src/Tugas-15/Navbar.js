import React, { useContext } from 'react';
import './Navbar.css';
import { ThemeContext } from './ThemeContext';
import ToggleTheme from './ToggleTheme';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Tugas9 from "../Tugas-9/Tugas9"
import Tugas10 from "../Tugas-10/Tugas10"
import Tugas11 from "../Tugas-11/Clock"
import Tugas12 from "../Tugas-12/DaftarBuah"
import Tugas13 from "../Tugas-13/DaftarBuah"
import Tugas14 from "../Tugas-14/Buah"




const Navbar = () => {
    const [theme,] = useContext(ThemeContext);

    return (
        <Router>
            <>
                <header class={theme}>
                    <nav>
                        <ul className={(theme) + "ul"}>
                            <li className={(theme) + "li"}>
                                <Link to="/Tugas-9" className={(theme) + "a"}>Tugas 9</Link>
                            </li>
                            <li className={(theme) + "li"}>
                                <Link to="/Tugas-10" className={(theme) + "a"}>Tugas 10</Link>
                            </li>
                            <li className={(theme) + "li"}>
                                <Link to="/Tugas-11" className={(theme) + "a"}>Tugas 11</Link>
                            </li>
                            <li className={(theme) + "li"}>
                                <Link to="/Tugas-12" className={(theme) + "a"}>Tugas 12</Link>
                            </li>
                            <li className={(theme) + "li"}>
                                <Link to="/Tugas-13" className={(theme) + "a"}>Tugas 13</Link>
                            </li>
                            <li className={(theme) + "li"}>
                                <Link to="/Tugas-14" className={(theme) + "a"}>Tugas 14</Link>
                            </li>
                        </ul>
                    </nav>
                </header>
                <ToggleTheme />

                <Switch>

                    {/* contoh dengan tag dengan end tag */}
                    <Route path="/Tugas-9">
                        <Tugas9 />
                    </Route>
                    <Route path="/Tugas-10">
                        <Tugas10 />
                    </Route>

                    {/* contoh tag route tanpa end tag */}
                    <Route exact path="/Tugas-11" component={Tugas11} />
                    <Route exact path="/Tugas-12" component={Tugas12} />
                    <Route exact path="/Tugas-13" component={Tugas13} />
                    <Route exact path="/Tugas-14" component={Tugas14} />

                </Switch>

            </>
        </Router>
    )
}



export default Navbar;