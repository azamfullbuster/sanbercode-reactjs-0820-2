import React, { useContext } from 'react';
import { ThemeContext } from './ThemeContext';

const ToggleTheme = () => {
    const [, setTheme] = useContext(ThemeContext);

    const lightTheme = (event) => {
        let newTheme = event.target.value
        setTheme(newTheme);
    }

    const darkTheme = (event) => {
        let newTheme = event.target.value
        setTheme(newTheme);
    }

    return (
        <>
            <div>
                <button value={"light"} onClick={lightTheme} style={{ marginright: "5px", marginLeft: "5px" }}>Light</button>
                <button value={"dark"} onClick={darkTheme} style={{ backgroundColor: "black", color: "white" }}>Dark</button>
            </div>
        </>
    )
}

export default ToggleTheme