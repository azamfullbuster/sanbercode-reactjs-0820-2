import React from 'react';
import Navbar from './Navbar';
import { ThemeProvider } from './ThemeContext';

export default function App() {
    return (
        <ThemeProvider>
            <Navbar />
        </ThemeProvider>
    )
}