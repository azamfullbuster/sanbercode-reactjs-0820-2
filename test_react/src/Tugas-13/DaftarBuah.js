import React, { useState, useEffect } from 'react'
import axios from 'axios'

const DaftarBuah = () => {

    const [dataHargaBuah, setDataHargaBuah] = useState(null)
    const [input, setInput] = useState({ id: null, name: "", price: "", weight: null })

    useEffect(() => {
        if (dataHargaBuah === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
                .then(res => {
                    setDataHargaBuah(res.data)
                    console.log(res.data)
                })
        }
    }, [dataHargaBuah]);


    const submitForm = (event) => {
        event.preventDefault()

        if (input.id === null) {
            axios.post(`http://backendexample.sanbercloud.com/api/fruits`, { name: input.name, price: input.price, weight: parseInt(input.weight) })
                .then(res => {
                    var data = res.data
                    setDataHargaBuah([...dataHargaBuah, { id: data.id, name: data.name, price: data.price, weight: data.weight }])
                    setInput({ id: null, name: "", price: "", weight: "" })

                })
        } else {
            axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, { name: input.name, price: input.price, weight: parseInt(input.weight) })
                .then(res => {
                    var newDataBuah = dataHargaBuah.map(x => {
                        if (x.id === input.id) {
                            x.name = input.name
                            x.price = input.price
                            x.weight = input.weight
                        }
                        return x
                    })
                    setDataHargaBuah(newDataBuah)
                    setInput({ id: null, name: "", price: "", weight: "" })
                })

        }
    }

    const changeInput = (event) => {
        var value = event.target.value
        setInput({ ...input, [event.target.name]: value })
    }

    const deleteData = (event) => {
        var idBuah = parseInt(event.target.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
            .then(res => {
                var newDataBuah = dataHargaBuah.filter(x => x.id !== idBuah)
                setDataHargaBuah(newDataBuah)
            })

    }

    const editData = (event) => {
        var idBuah = parseInt(event.target.value)
        var buah = dataHargaBuah.find(x => x.id === idBuah)

        setInput({ id: idBuah, name: buah.name, price: buah.price, weight: buah.weight })

    }



    return (
        <>
            <h1 style={{ textAlign: "center" }}> Daftar Harga Buah</h1>
            <table style={{ border: "1px solid", width: "50%", margin: "0 auto" }}>
                <thead style={{ background: "#aaa" }}>
                    <tr>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody style={{ background: "coral" }}>
                    {
                        dataHargaBuah !== null && dataHargaBuah.map((item, index) => {
                            return (
                                <>
                                    <tr key={item.id}>
                                        <td>{item.name}</td>
                                        <td>{item.price}</td>
                                        <td>{item.weight / 1000} kg</td>
                                        <td style={{ textAlign: "center" }}>
                                            <button value={item.id} onClick={editData} style={{ marginright: "3px", marginLeft: "5px" }}>Edit</button>
                                            <button value={item.id} onClick={deleteData}>Delete</button>
                                        </td>
                                    </tr>
                                </>
                            )
                        })
                    }
                </tbody>
            </table>
            <br /> <br />
            <h1 style={{ textAlign: "center" }}>Form Data Buah</h1>
            <form onSubmit={submitForm}>
                <table style={{ width: "40%", margin: "0 auto" }}>
                    <tr>
                        <th>Nama Buah</th>
                        <td><input required type="text" name="name" value={input.name} onChange={changeInput} /></td>
                    </tr>
                    <tr>
                        <th>Harga Buah</th>
                        <td><input required type="number" name="price" value={input.price} onChange={changeInput} /></td>
                    </tr>
                    <tr>
                        <th>Berat Buah (gr)</th>
                        <td><input required type="number" name="weight" value={input.weight} onChange={changeInput} /></td>
                    </tr>
                    <tr>
                        <td><br /></td>
                    </tr>
                    <tr>
                        <td><button class="button4">Submit</button></td>
                    </tr>
                </table>
            </form>
        </>
    )
}

export default DaftarBuah
