import React from 'react';

class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            time: 100,
            showTime: true
        };
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentDidUpdate() {
        if (this.state.showTime == true) {
            if (this.state.time <= 0) {
                this.componentWillUnmount()
                this.setState({
                    showTime: false
                })
            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            date: new Date(),
            time: this.state.time - 1
        });
    }

    render() {
        return (
            <>
                {this.state.showTime && (
                    <>
                        <div>
                            <h2>Sekarang Jam : {this.state.date.toLocaleTimeString('en-US')}</h2>
                            <h3>Hitung Mundur : {this.state.time}</h3>
                        </div>
                    </>
                )}
            </>
        );
    }
}

export default Clock