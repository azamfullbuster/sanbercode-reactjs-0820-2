import React from 'react';
import './Tugas10.css';
import ItemBuah from './ItemBuah'

let dataHargaBuah = [
    { nama: "Semangka", harga: 10000, berat: 1000 },
    { nama: "Anggur", harga: 40000, berat: 500 },
    { nama: "Strawberry", harga: 30000, berat: 400 },
    { nama: "Jeruk", harga: 30000, berat: 1000 },
    { nama: "Mangga", harga: 30000, berat: 500 }
]

class TableBuah extends React.Component {
    render() {
        return (
            <>
                <div className="inner">
                    <h1>Tabel Harga Buah</h1>
                    <table border="1px" className="listtable">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Harga</th>
                                <th>Berat</th>
                            </tr>
                        </thead>
                        <tbody style={{ background: "coral" }}>
                            {dataHargaBuah.map((el, index) => {
                                return (
                                    <>
                                        <ItemBuah item={el} key={index} />
                                    </>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </>
        )
    }
}

export default TableBuah
