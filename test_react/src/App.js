import React from 'react';
import './App.css';
// import Form from './Tugas-9/Tugas9';
// import TableBuah from './Tugas-10/Tugas10';
// import Clock from './Tugas-11/Clock';
// import DaftarBuah from './Tugas-13/DaftarBuah';
// import Buah from './Tugas-14/Buah'
import Routes from './Tugas-15/Routes';

function App() {
  return (
    <div>
      {/* <Clock />
      <br /> <br />
      <Form />
      <br /> <br />
      <TableBuah /> 
      <DaftarBuah />*/}
      <Routes />
    </div>
  );
}

export default App;
